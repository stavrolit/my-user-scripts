// ==UserScript==
// @name       Aukro Facilities
// @version    0.1
// @description  Aukro Facilities
// @run-at document-end
// @include http://aukro.ua/*
// @include https://aukro.ua/*
// ==/UserScript==

var AttachStyle = function() {
	var c = {
		STYLE_ID: "user-scripts-style",
		createStyle: function() {
			var b = document.createElement("style");
			b.type = "text/css";
			return b;
		},
		appendCssToStyle: function() {
			var b, a;
			if (c.createStyle().styleSheet) {
				b = function(a) {
					return a.styleSheet.cssText;
				};
				a = function(a, b) {
					a.styleSheet.cssText = b;
				};
			} else {
				b = function(a) {
					return a.innerHTML;
				};
				a = function(a, b) {
					a.innerHTML = b;
				};				
			}
			c.appendCssToStyle = function(c, d) {
				a(d, b(d) + c);
			};
			return c.apply(this, arguments);
		}
	};
	return function(b) {
		var a = document.getElementById(c.STYLE_ID);
		var d = function() {
			this.parentNode.insertBefore(a, this);
		};

		if (a) {
			a = c.createStyle();
			a.id = c.STYLE_ID;
			d.call(document.getElementsByTagName('head')[0].lastChild);
		}
		c.appendCssToStyle(b, a);
	};
};

var builder = {

	qualifyURL: function(url) {
		var img = document.createElement('img');
		img.src = url;
		url = img.src;
		img.src = '';
		return url;
	},

	linkParser: function(jLink) {

		var urlObj = new URL(this.qualifyURL(jLink.attr('href')));
		var mainPart = urlObj.origin + urlObj.pathname;
		var params = urlObj.search;
		var hash = urlObj.hash;
		var resultObj;

		if (params.indexOf('userScript=donotprocess') < 0) {
			return {};
		}

		resultObj = {
			isSendMailToUserPage: /\/SendMailToUser\.php$/.test(mainPart),
			isWonListShowAllPage: /\/myaccount\/won\.php\/showAll,1/.test(mainPart),
			mainPart: mainPart,
			params: params
		};
		return resultObj;

	},

	// Restyling main site pages table (width)
	sendMailPrepopulationButton: function() {

		jQuery('td.formsubmit')
			.append(
				'<div class="button" style="' + 
					'width:165px;' +
					'height:20px;' +
					'background-image:linear-gradient(#74bbcc,#6acccb);' +
					'float: left;' +
					'text-align:center;' +
					'line-height:20px;' +
					'font-size:16px;' +
					'cursor:pointer;' +
					'border:1px solid #FFBA2D;' +
					'color:white' +
				'">Additional Instructions</div>')
			.find('div.button').click(function() {
				jQuery(this).closest('table').find('textarea.form').text('Здравствуйте,\n\nприобрел у вас лот(ы):\n\nСообщите пожалуйста номер карты для перевода, или, если это возможно, вышлите Новой Почтой с наложенным платежом.\n\nМои данные:\nг. Днепропетровск\nНовая Почта №16\nСаржинский Александр\n+380954057382\n\nС уважением,\nАлександр');
			});

	},

	applyClientFilter: function() {

		var sum = 0;
		jQuery('span.uname:contains("ssr-records")')
			.closest('tr')
			.find('.nowrap.toright strong:first')
			.each(function(){
				sum += Number(this.innerHTML.replace(' грн.', '').replace(',', '.'));
			});

	},

	init: function() {

		var currentJLink = jQuery(unsafeWindow.location),
			currentJlinkParsed = this.linkParser(currentJLink),
			isThisIframe = (unsafeWindow !== unsafeWindow.top);

		// We do not process pages inside Iframes
		if (isThisIframe) {
			return;
		}

		// Send Mail To User Prepoputation Button Creation
		if (currentJlinkParsed.isSendMailToUserPage) {
			this.sendMailPrepopulationButton();
		}

		// Create filters for Won page
		if (currentJlinkParsed.isWonListShowAllPage) {
			this.applyClientFilter();
		}

	}

};

var waiterInterval = setInterval(function() {

	if (typeof unsafeWindow.jQuery !== 'function') {
		return false;
	}
	clearInterval(waiterInterval);

	builder.init();

}, 50);