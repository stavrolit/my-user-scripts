// ==UserScript==
// @name       Kinopoisk Facilities
// @version    0.1
// @description  Kinopoisk Facilities
// @require https://raw.github.com/rafjaf/ClipCopy-for-TM/master/lib/installGM_setClipboard.js
// @run-at document-body
// @include http://www.kinopoisk.ru/*
// @include https://www.kinopoisk.ru/*
// ==/UserScript==

var AttachStyle = function() {
	var c = {
		STYLE_ID: "user-scripts-style",
		createStyle: function() {
			var b = document.createElement("style");
			b.type = "text/css";
			return b
		},
		appendCssToStyle: function() {
			var b, a;
			"styleSheet" in c.createStyle() ? (b = function(a) {
				return a.styleSheet.cssText
			}, a = function(a, b) {
				a.styleSheet.cssText = b
			}) : (b = function(a) {
				return a.innerHTML
			}, a = function(a, b) {
				a.innerHTML = b
			});
			return (c.appendCssToStyle = function(c, d) {
				a(d, b(d) + c)
			}).apply(this, arguments)
		}
	};
	return function(b) {
		var a = document.getElementById(c.STYLE_ID);
		a || (a = c.createStyle(), a.id = c.STYLE_ID, function() {
			this.parentNode.insertBefore(a, this)
		}.call(document.getElementsByTagName("head")[0].lastChild));
		c.appendCssToStyle(b, a)
	}
};

var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

var getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

var builder = {

	qualifyURL: function(url) {
		var img = document.createElement('img');
		img.src = url;
		url = img.src;
		img.src = '';
		return url;
	},

	linkParser: function(jLink) {

		var urlObj = new URL(this.qualifyURL(jLink.attr('href'))),
			mainPart = urlObj.origin + urlObj.pathname,
			params = urlObj.search,
			hash = urlObj.hash,
			resultObj;

		if (params.indexOf('userScript=donotprocess') < 0) {
			resultObj = {
				isMoviePage: /\/film\//.test(mainPart),
				mainPart: mainPart,
				params: params
			};
		} else {
			resultObj = {};
		}

		return resultObj;

	},

	moviePageRedesign: function() {

		AttachStyle()(
			'body .film-header {display: none}'
		);

	},

	init: function() {

		var currentJLink = jQuery(unsafeWindow.location),
			currentJlinkParsed = this.linkParser(currentJLink),
			isThisIframe = (unsafeWindow !== unsafeWindow.top);

		// We do not process pages inside Iframes
		if (isThisIframe) {
			return;
		}

		// Movie Page processing
		if (currentJlinkParsed.isMoviePage) {
			this.moviePageRedesign();
		}

	}

};

var waiterInterval = setInterval(function() {

	if (typeof unsafeWindow.jQuery !== 'function') {
		return false
	}
	clearInterval(waiterInterval);

	builder.init();

}, 50);