// ==UserScript==
// @name       Discogs live basket info
// @version    0.1
// @description  on Discogs marketplace item page shows whether seller is present in your basket
// @run-at document-end
// @include http://www.discogs.com/marketplace?release_id=*
// @include http://www.discogs.com/sell/list?release_id=*
// @include http://www.discogs.com/sell/release/*
// @include https://www.discogs.com/marketplace?release_id=*
// @include https://www.discogs.com/sell/list?release_id=*
// @include https://www.discogs.com/sell/release/*
// ==/UserScript==

var waiterInterval = setInterval(function() {
	
	if (!unsafeWindow.jQuery) {
		return false
	}
	clearInterval(waiterInterval);

	var $ = unsafeWindow.jQuery,
		console = unsafeWindow.console,
		document = unsafeWindow.document,
		protocol = unsafeWindow.location.protocol;
	
	var AttachStyle = function() {
		var c = {
			STYLE_ID: "user-scripts-style",
			createStyle: function() {
				var b = document.createElement("style");
				b.type = "text/css";
				return b
			},
			appendCssToStyle: function() {
				var b, a;
				"styleSheet" in c.createStyle() ? (b = function(a) {
					return a.styleSheet.cssText
				}, a = function(a, b) {
					a.styleSheet.cssText = b
				}) : (b = function(a) {
					return a.innerHTML
				}, a = function(a, b) {
					a.innerHTML = b
				});
				return (c.appendCssToStyle = function(c, d) {
					a(d, b(d) + c)
				}).apply(this, arguments)
			}
		};
		return function(b) {
			var a = document.getElementById(c.STYLE_ID);
			a || (a = c.createStyle(), a.id = c.STYLE_ID, function() {
				this.parentNode.insertBefore(a, this)
			}.call(document.getElementsByTagName("head")[0].lastChild));
			c.appendCssToStyle(b, a)
		}
	};
	AttachStyle()('li.no_highlight a:visited, li.no_highlight a:active, li.no_highlight a:hover, li.no_highlight a {color: #080} .total_price_us {color: red} li.heading_us {font-size: 15px} body table.mpitems tr td.seller_info {width: auto}');
	
	
	$.ajax(protocol + '//www.discogs.com/sell/cart', {
		success : function(text) {
			
			text = text.replace(/[\r\n]/g, '').replace(/<script[^>]*>.*?<\/script>/g, '').replace(/<head[^>]*>.*?<\/head>/g, '').replace(/<img[^>]*>/g, '');
			
			var storesReceivedNodes = $(text).find('a.linked_username'),
				storesActualNodes = $('td.seller_info .seller_label').parent().find('strong a:first'),
				storesReceivedObject = {},
				storesActualArray = [];
			
			storesReceivedNodes.each(function() {
				
				var self = $(this),
					selfHTML = $.trim(self.html());
				
				storesReceivedObject[selfHTML] = {
					'itemsQuantity' : self.closest('div.order_container').find('tr.order_row').length,
					'totalPrice' :  $.trim(self.closest('div.order_container').find('tr.order_subtotal').text().replace(/Subtotal/g, '')),
					'productsAddedLinks' : [],
					'productsAddedPrices' : [],
					'productInfoFull' : []
				};
				
				self.closest('div.order_container').find('tr.order_row').find('td.order_item_info').each(function() {
					var textData = this.innerHTML.replace(/Media: /g, '').replace(/Sleeve: /g, '').replace(/<br>/g, ' - ').replace(/Mint \(M\)/g, '(M)').replace(/Very Good Plus \(VG\+\)/g, '(VG+)').replace(/Near Mint \(NM or M-\)/g, '(NM)');
					storesReceivedObject[selfHTML]['productsAddedLinks'].push(textData)
				});
				
				self.closest('div.order_container').find('tr.order_row').find('td.price').each(function() {
					var textData = this.innerHTML.replace(/<br>/g, ' - ');
					storesReceivedObject[selfHTML]['productsAddedPrices'].push('<b>' + textData + '</b>')
				});
				
				
				if (storesReceivedObject[selfHTML]['productsAddedPrices'].length != storesReceivedObject[selfHTML]['productsAddedLinks'].length) {
					console.log('some basket problems')
				}
				
				$.each(storesReceivedObject[selfHTML]['productsAddedLinks'], function(index, value) {
					storesReceivedObject[selfHTML]['productInfoFull'].push(storesReceivedObject[selfHTML]['productsAddedLinks'][index] + ' - ' + storesReceivedObject[selfHTML]['productsAddedPrices'][index])
				});

			});
			
			storesActualNodes.each(function() {
				
				var self = $(this),
					selfHTML = $.trim(self.html()),
					quantity = '',
					totatlPrice = 0;
				
				if (storesReceivedObject[selfHTML]) {
					quantity = storesReceivedObject[selfHTML]['itemsQuantity'];
					totatlPrice = storesReceivedObject[selfHTML]['totalPrice'];
					self.closest('ul').append('<li class="no_highlight heading_us"><br><b>' + quantity + ' item' + (quantity == 1 ? '' : 's') + ' already in basket:' + '</b> - <font class="total_price_us">' + totatlPrice + '</font><br><br>')
									  .append('<li class="no_highlight">' + storesReceivedObject[selfHTML]['productInfoFull'].join('<br>') + '</li>')
				}

			});

		}
	})
	
}, 50);