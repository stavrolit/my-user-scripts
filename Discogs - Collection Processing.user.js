// ==UserScript==
// @name       Discogs "My Collection" page
// @version    0.1
// @description  stores collection items in a clipboard
// @require https://raw.github.com/rafjaf/ClipCopy-for-TM/master/lib/installGM_setClipboard.js
// @run-at document-end
// @include http://www.discogs.com/*
// ==/UserScript==

if (unsafeWindow.location.pathname.match(/\/collection$/)) {

	var waiterInterval = setInterval(function() {
		
		if (!unsafeWindow.jQuery) {
			return false
		}
		clearInterval(waiterInterval);

		var $ = unsafeWindow.jQuery,
			console = unsafeWindow.console,
			document = unsafeWindow.document;

		var getCollectionData = function() {

			var releasesString = '';

			$('.release_list_table span.release_title').each(function() {

				var childNodes = $(this).contents(),
					closestParent = $(this).closest('tr'),
					artistsName = '',
					releaseTitle = '',
					labelsTitle = '',
					labelsCodes = '',
					releaseFormat = '',
					releaseYear = '';

				childNodes.each(function() {

					var nextNode = $(this).next()[0],
						currentNodeType = this.nodeType,
						currentClassName = this.className,
						currentNodeName = this.nodeName.toLowerCase(),
						currentNodeHref = this.href;

					if (currentNodeName === 'a' && currentNodeHref.indexOf('/artist/') > -1) {
						artistsName += $(this).text();
					} else if (
						currentNodeType === 3 &&
						nextNode &&
						nextNode.nodeName.toLowerCase() === 'a' &&
						nextNode.href.indexOf('/artist/') > -1) {

						artistsName += $(this).text();

					} else if (currentNodeName === 'span' && currentClassName === 'release_title_link') {
						releaseTitle += $(this).text();
					} else if (currentNodeName === 'a' && currentNodeHref.indexOf('/label/') > -1) {
						labelsTitle += ((labelsTitle ? ', ' : '') + $(this).text());
					} else if (currentNodeType === 3 && !nextNode) {
						labelsCodes += $.trim($(this).text()).replace(/- |\)/g, '');
					}

				});

				releaseFormat = closestParent.find('td[data-header="Format"]').text();
				releaseYear = closestParent.find('td[data-header="Year"]').text();
				releasesString += releaseFormat + '\t' + artistsName + '\t' + releaseTitle + '\t' + labelsTitle + '\t' + labelsCodes + '\t' + releaseYear + '\n';

			});

			console.log(releasesString);

			GM_setClipboard(releasesString);

		};

		var newButton = $('<a class="button" href="#clipboard" style="' +
					'width: 90px;' +
					'padding: 7px;' +
					'position: absolute;' +
					'right: -120px;' +
					'top: 7px;' +
					'text-align: center;' +
					'text-decoration: none;' +
					'background-color: #C24AAF' +
				'">Copy to Clipboard</a>');

		$('button.release_list_remove').after(newButton);

		newButton.on('click', function(e) {
			e.stopImmediatePropagation();
			e.preventDefault();
			getCollectionData();
		});

	}, 50);

}