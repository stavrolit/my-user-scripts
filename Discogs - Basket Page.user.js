// ==UserScript==
// @name       Discogs basket page
// @version    0.1
// @description  on Discogs basket page immediately hides Ignored items
// @run-at document-end
// @include http://www.discogs.com/sell/cart?*
// @include http://www.discogs.com/sell/cart/?*
// @include http://www.discogs.com/sell/cart
// @include http://www.discogs.com/sell/cart/
// @include https://www.discogs.com/sell/cart/?*
// @include https://www.discogs.com/sell/cart?*
// @include https://www.discogs.com/sell/cart
// @include https://www.discogs.com/sell/cart/
// ==/UserScript==

var waiterInterval = setInterval(function() {

	if (!unsafeWindow.jQuery) {
		return false;
	}
	clearInterval(waiterInterval);

	var $ = unsafeWindow.jQuery,
		console = unsafeWindow.console;

	$('.order_instructions').parent()
		.append(
			'<div class="button" style="' +
			'width:197px;' +
			'height:40px;' +
			'background-image:linear-gradient(#74bbcc,#6acccb);' +
			'margin-top:5px;' +
			'text-align:center;' +
			'line-height:40px;' +
			'font-size:20px;' +
			'cursor:pointer;' +
			'color:white' +
			'">Additional Instructions</div>')
		.find('div.button').click(function() {
			$(this).parent().find('.order_instructions')
				.text('Please provide the cheapest delivery price possible.\nPlease declare the lowest price possible on the package.\nPlease mention my phonenumber on the package: +380954057382\n\nThanks in advance,\nAleksandr');
		});

	$('.modified-cart-ignore').unbind().click(function() {

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: '/sell/modified-cart/remove/' + $(this).data('release-id')
		});
		$(this).html('ok');
		if ($('#modified-cart li:visible').length <= 1) {
			$("#modified-cart").hide();
		} else {
			$(this).parent().hide();
		}

	});

	$('.linked_username').each(function() {
		var currentSeller = ($(this).attr('href') || '').replace(/\/user\//g, '');
		$(this).closest('div').append(
			'<a style="margin-left:10px" href="http://www.discogs.com/seller/' +
			currentSeller +
			'/mywants?sort=posted%2Cdesc&limit=250">More items from my wantlist</a>'
		);
	});

}, 50);