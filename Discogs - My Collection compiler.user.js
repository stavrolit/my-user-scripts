var listText = '';

jQuery('.release_title').each(function () {

	var currentSpan = $(this),
		artistLinks = currentSpan.find('a[href*="/artist/"]'),
		artist = ''
		release = currentSpan.find('a[href*="/release/"]').text(),
		labelLinks = currentSpan.find('a[href*="/label/"]'),
		labelInfo = currentSpan.contents().last().text(),
		labelName = '',
		catNumber = '',
		releaseFormatInfo = currentSpan.parent().next('td').text(),
		releaseFormat = '';

	artistLinks.each(function () {
		artist += $(this).text() + ' / '
	});
	artist = artist.replace(/ \/ $/, '');

	labelLinks.each(function () {
		labelName += $(this).text() + ', '
	});
	labelName = labelName.replace(/, $/, '');

	catNumber = jQuery.trim(labelInfo).replace(/^- /, '').replace(/\s*\)$/, '');

	listText += (artist + '	' + release + '	' + labelName + '	' + catNumber + '\n')

});

console.log(listText);
copy(listText)
